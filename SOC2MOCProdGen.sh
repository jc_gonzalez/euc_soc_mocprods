#!/bin/bash
##############################################################################
# File       : SOC2MOCProdGen.sh - SOC to MOC Products Generator
# Version    : 3.0
# Date       : 2020/10/20
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Launches the SOC to MOC Products Generator Interactive Tool
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

export PYTHONPATH=${SCRIPTPATH}/mocprods

PYTHON=python3

${PYTHON} ${SCRIPTPATH}/mocprods/mocprods_gui.py

exit 0
