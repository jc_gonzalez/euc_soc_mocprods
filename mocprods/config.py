#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Configuration parameters
"""

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

SOCprodsforMOC_image = os.path.join(_filedir_, 'img/SOCprodsforMOC.gif')

SCS2PORxslt = os.path.join(_filedir_, 'xslt/scs_evt_2_por.xslt')
FilesRepository = os.path.join(_filedir_, 'files')
DBFile = os.path.join(FilesRepository, 'index.db')


if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
