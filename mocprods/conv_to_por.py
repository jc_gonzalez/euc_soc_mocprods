#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module to define class for conversion of SCS Events file into POR
format, by using a XSLT (eXtensible Stylesheet Language Transformation)
"""
import tkinter as tk                # python 3
import tkinter.ttk as ttk           # python 3

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys

from config import SCS2PORxslt
from gui_elements import LabelTemp
from tools import run

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class ConvertSCSOutputToPOR(ttk.Frame):
    """
    Class for conversion of SCS Events file into POR format, by using
    a XSLT (eXtensible Stylesheet Language Transformation)
    """
    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        self.controller = controller

        lfrm1 = ttk.LabelFrame(self, text='Convert SCS Output to POR')

        self.inputFile = tk.StringVar()
        self.outputFile = tk.StringVar()

        frm11 = ttk.Frame(lfrm1)
        frm12 = ttk.Frame(lfrm1)

        ttk.Label(frm11, text='Input SCS file (XML)   ')\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edInputFile = ttk.Entry(frm11, textvariable=self.inputFile)
        self.edInputFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm11, text='...', width=2,
                   command=lambda: controller.setPath(self.inputFile, folder=False))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm11.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm12, text='Output POR file (XML)')\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edOutputFile = ttk.Entry(frm12, textvariable=self.outputFile)
        self.edOutputFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm12, text='...', width=2,
                   command=lambda: controller.setPath(self.outputFile, folder=False, save=True))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm12.pack(expand=tk.N, fill=tk.X)

        self.lblResult = LabelTemp(lfrm1, 5.0)
        self.lblResult.pack(fill=tk.BOTH, expand=tk.YES)

        ttk.Label(lfrm1, text='').pack(fill=tk.BOTH, expand=tk.YES) # spacer

        btnGo = ttk.Button(lfrm1, text="Convert",
                           command=lambda: self.convertToPOR())
        btnBack = ttk.Button(lfrm1, text="<- back", #text="\U000021b2",
                             command=lambda: controller.show_frame("StartPage"))

        btnGo.pack(side=tk.RIGHT, padx=2, pady=10)
        btnBack.pack(side=tk.LEFT, padx=2, pady=10)

        lfrm1.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)

    def convertToPOR(self, sequenceName='Sequence'):
        """Use XSLT to convert output from SCS to POR format"""
        logger.info('Preparing execution of SCS to POR XSLT on input file')
        inFile = self.inputFile.get()
        outFile= self.outputFile.get()
        tmpFile = f'{outFile}.tmp'
        optList = ['--timing',
                   '--stringparam', 'sequence_name', sequenceName,
                   '--output', tmpFile]
        opts = ' '.join(optList)

        logger.info('Conversion of SCS Events file to POR . . .')
        cmd1 = f'/usr/bin/xsltproc {opts} {SCS2PORxslt} {inFile}'
        info, exitCode = run(cmd1)
        hasErrors = "ERROR" in info
        logInfo1 = str(info).split('\n')

        if exitCode == 0 and not hasErrors:
            optList = ['--format',
                       '--output', outFile]
            opts = ' '.join(optList)

            logger.info('Cleanning up and reformatting . . .')
            cmd2 = f'/usr/bin/xmllint {opts} {tmpFile}'
            info, exitCode = run(cmd2)
            logInfo2 = str(info).split('\n')

            if exitCode != 0:
                msg = "ERROR: Couldn't reformat the output from the XSLT transformation"
                logInfo2 = [info, msg]
                self.lblResult.config(foreground='#AA0000')
                self.lblResult.set(msg)

            self.lblResult.config(foreground='#00AA00')
            self.lblResult.set('Conversion to POR successful!')

        else:
            msg = "FATAL: Errors in the execution of the XSLT transformation"
            logInfo1 = [info, msg]
            cmd2 = ''
            logInfo2 = []

            self.lblResult.config(foreground='#AA0000')
            self.lblResult.set(msg)

        logInfo = [f'>> {cmd1}'] + logInfo1 + ['', f'>> {cmd2}'] + logInfo2
        self.controller.showLog(logInfo)

        logger.info('Done.')


if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
