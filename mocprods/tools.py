#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Small set of generic tools
"""

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys, errno
import subprocess
import hashlib

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

def getContentOfFile(file=None):
    """
    Gets the content of a text file
    """
    if os.path.exists(file) and os.path.isfile(file):
        with open(file, 'r') as f:
            return f.read()
    else:
        return ''

def run(cmd):
    completed = subprocess.run(cmd.split(' '),
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                               universal_newlines=True)
    try:
        completed.check_returncode()
    except subprocess.CalledProcessError as ee:
        return 'ERROR!\n' + ee.stdout, completed.returncode
    return completed.stdout, completed.returncode

def createDirIfNotExist(the_dir=None):
    """
    Creates a directory if it does not yet exist
    """
    if the_dir:
        if not os.path.exists(the_dir):
            try:
                os.makedirs(the_dir)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

def change_bgcolor(wdg, c):
    change_bgcolors(wdg.winfo_children(), c)

def change_bgcolors(wdgl, c):
    for w in wdgl:
        if hasattr(w, 'bg'): w.configure(bg=c)

def md5sum(filename):
    """
    Compute MD5 of a file
    :param filename: the file name
    :return: the MD5 hash
    """
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        while True:
            buf = f.read(4096) # 128 is smaller than the typical filesystem block
            if not buf: break
            d.update(buf)
    return d.hexdigest()

if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
