#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions for file management and file naming according to the agreed conventions

Latest naming convention is:

- PORs:
  POR__ESAC_<plancycle+version>_[<ssrfd_reference>_]<optional text>_<counter>.EUC
- CRFGs (POR container):
  CRFG_ESAC_<plancycle+version>_<ssrfd_reference>_<optional text>_<counter>.ZIP
  Included in CRFG is also the manifest as per PLID (with "MAN" instead of "POR")

An example with multiple POR’s inside would result for example in a delivery of a single CRFG:
CRFG_ESAC_001V1_001V1_<optional text>_<counter>.ZIP, which
contains:
     POR__ESAC_001V1_001V1_<optional text>_0001.EUC
     POR__ESAC_001V1_001V1_<optional text>_0002.EUC
     POR__ESAC_001V1_001V1_<optional text>_0003.EUC
     MAN__ESAC_001V1_001V1_<optional text>_0001.EUC

- SRR-FD:
  SRR__ESAC_<plancycle+version>_<optional text>_<counter>.<EXT>

<plancycle+version>: planing cycle as "000" + "V" + version as "0": 000V0

Max. filename length is 64 bytes.
"""

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys
import re
import pickle
import datetime
import copy
import collections
import shutil

from enum import Enum
from tools import md5sum
from xml.etree import ElementTree as ET

from config import FilesRepository
from pprint import pprint

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class FileType(Enum):
    SSRFD = 0
    POR = 1
    EVENTS = 2
    CRFG = 3
    MANIFEST = 4


MaxFileNameLength = 64
MaxFileNameLengthNoCounter = MaxFileNameLength - 5

FileTypeStr = ['ssrfd', 'por', 'events', 'crfg', 'manifest']

FileTypeInfo = {FileType.SSRFD.name:    ('SSR_', 'EUC'),
                FileType.POR.name:      ('POR_', 'EUC'),
                FileType.EVENTS.name:   ('EVT_', 'EUC'),
                FileType.CRFG.name:     ('CRFG', 'ZIP'),
                FileType.MANIFEST.name: ('MAN_', 'EUC')}

FileTypeMembers = list(FileType) #FileType.__members__.items()

FileTypeNumber = {m.name: x for x, m in enumerate(FileTypeMembers)}

FileTypeId = {m.name: FileTypeStr[x] for x, m in enumerate(FileTypeMembers)}

InternalDataStore = "index.db"

InternalDataEmpty = {
    'files': [],
    'ssrfd': [],
    'por': [],
    'events': [],
    'crfg': [],
    'info': [],
    'info_name': {},
    'versions': {'ssrfd': {},
                 'por': {}},
    'counters': {'ssrfd': 0,
                 'por': 0,
                 'events': 0,
                 'crfg': 0,
                 'manifest': 0}
}

InternalData = copy.deepcopy(InternalDataEmpty)

InternalDataBackup = []

def initialize_db(conn=None):
    """
    Initializes tables in SQLite3 DB
    :return: -
    """
    if conn is None:
        return False

    # Initialize tables
    c = conn.cursor()
    c.execute("""CREATE TABLE files
                             (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                              date TEXT DEFAULT CURRENT_TIMESTAMP NOT NULL, 
                              name TEXT NOT NULL, 
                              filetype INTEGER NOT NULL, 
                              cycle INTEGER NOT NULL, 
                              version INTEGER DEFAULT 1 NOT NULL, 
                              refersTo INTEGER,
                              stored TEXT DEFAULT CURRENT_TIMESTAMP NOT NULL)""")
    c.execute("""CREATE VIEW ssrfd_files AS 
                             SELECT * FROM files WHERE type='ssrfd' """)
    c.execute("""CREATE VIEW por_files AS 
                             SELECT * FROM files WHERE type='por' """)
    c.execute("""CREATE TABLE counters
                             (filetype INTEGER PRIMARY KEY, 
                              counter INTEGER)""")

    records = [[t, 0] for n, t in FileTypeNumber.items()]
    c.executemany("""INSERT INTO counters('filetype', 'counter')
                                 VALUES (?, ?)""", records)

    conn.commit()
    conn.close()

    return True

def setInternalDataStore(pckl=None):
    """Set name of the pickle internal data store"""
    global InternalDataStore
    if pckl is None:
        return False
    InternalDataStore = pckl
    return True

def getListOfFiles(filetype:FileType):
    """Return list with files stored of a given type"""
    global InternalData
    return InternalData[str(FileTypeId[filetype.name])]

def initialize_pickle(pckl=None, dbExists=True):
    """Initializes structure in Pickle file"""
    if not setInternalDataStore(pckl): return False
    print(pckl, dbExists)
    if dbExists:
        return loadInternalData()
    else:
        return saveInternalData()

def transaction_start():
    """Creates a copy of the internal data structure"""
    global InternalData, InternalDataBackup
    InternalDataBackup.append(copy.deepcopy(InternalData))

def transaction_commit():
    """Saves the changes of internal data to disk"""
    global InternalDataBackup
    saveInternalData()
    InternalDataBackup.clear()

def transaction_rollback():
    """Recover internal data state at start of transaction"""
    global InternalData, InternalDataBackup
    InternalData = copy.deepcopy(InternalDataBackup.pop())


def saveInternalData():
    """Save the current values of the internal data structure in the
    internal data store"""
    global InternalDataStore, InternalData
    try:
        with open(InternalDataStore, 'wb') as pcklFile:
            pickle.dump(InternalData, pcklFile, pickle.HIGHEST_PROTOCOL)
        return True
    except:
        return False

def loadInternalData():
    """Load the value of the internal data structure from the
    internal data store"""
    global InternalDataStore, InternalData
    try:
        with open(InternalDataStore, 'rb') as pcklFile:
            InternalData = pickle.load(pcklFile)
        return True
    except:
        return False

def dumpInternalData():
    """Show in console the contents of the internal data"""
    global InternalData
    pprint(InternalData)

def clearInternalData():
    """Show in console the contents of the internal data"""
    global InternalData
    InternalData = copy.deepcopy(InternalDataEmpty)
    dumpInternalData()

def getPlanningCycleFromFileToLoad(name):
    """
    Tries to get the planning cycle number from the file name of a file
    that the user wants to load
    :param name: the file name
    :return: the planning cycle, or None if unsuccessful
    """
    m = re.search('\d+', name)
    if m is None:
        return None
    else:
        return int(m.group(0))

def getNextVersionForPlanningCycle(planCycle, filetype=FileType.POR):
    """
    Returns the version number to be used for the specified planning cycle
    :param planCycle: the planning cycle
    :param filetype: the type of file, ssrfd or por
    :return:
    """
    pc, ftype = getPlCyclFtypeStr(planCycle, filetype)
    if pc in InternalData['versions'][ftype]:
        version = InternalData['versions'][ftype][pc] + 1
    else:
        version = 1
    return version

def setLastVersionForPlanningCycle(planCycle, filetype=FileType.POR, ver=0):
    """
    Sets last version used for a given planning cycle
    :param planCycle: the planning cycle
    :param filetype: the type of file, ssrfd or por
    :param ver: the last version registered
    :return: -
    """
    pc, ftype = getPlCyclFtypeStr(planCycle, filetype)
    InternalData['versions'][ftype][pc] = ver

def generateFileName(filetype, planCycle=0, ver=0, addPlanCycle=None, addVer=None, text='', counter=None, addDate=None):
    """
    Generate file name according to file name specs.
    :param text:
    :param filetype: the file type
    :param planCycle: the planning cycle number
    :param ver: the version numbe is None
    :param addPlanCycle: the planning cycle of the referenced file, or None
    :param addVer: the version of the referenced file, or None
    :param counter: the counter to add, if any
    :param addDate: the generation date, if used, or None
    :return: the filenameure is as follows:
    """
    plcyclRef = ('_' * 5) if addPlanCycle is None else f'_{addPlanCycle:03d}V{addVer:01d}'
    plcycl = f'_{planCycle:03d}V{ver:01d}'
    prefix, ext = FileTypeInfo[filetype.name]
    mainSec = f'{prefix}_ESAC{plcycl}{plcyclRef}_'
    dateSec = '' if addDate is None else datetime.datetime.now().strftime('_%Y%m%dT%H%M%S')
    textSec = ''.join([c if c != ' ' else '_' for c in text]) if text else ''
    remainingText = '_' * (MaxFileNameLengthNoCounter - (len(mainSec) + len(textSec) + len(dateSec)))
    counterText = '' if not counter else f'_{counter:04d}'
    return os.path.join(FilesRepository, f'{mainSec}{textSec}{remainingText}{dateSec}.{ext}')

def getPlCyclFtypeStr(planCycle, filetype=FileType.POR):
    """
    Get strings from planning cycle and file type
    :param planCycle: the planning cycle
    :param filetype: the type of file, ssrfd or por
     :return: pl, ty
    """
    return str(planCycle), str(FileTypeId[filetype.name])

def storeFile(name, filetype, planCycle=None, ver=None, text=''):
    """
    Stored a single file in the Pickle file
    :param text:
    :param name: the file name
    :param filetype: the type of file, ssrfd or por
    :param planCycle: the planning cycle
    :param ver: the last version registered
    :return:
    """
    # 1. Obtain new file name according to the file naming conv.
    newName = generateFileName(filetype, planCycle, ver, text=text, addDate=True) \
              if filetype in [FileType.SSRFD, FileType.POR] \
              else os.path.join(FilesRepository, os.path.basename(name))

    # 2. Copy file to repository
    shutil.copy(name, newName)

    # 3. Store info in DB
    pc, ftype = getPlCyclFtypeStr(planCycle, filetype)
    if newName in InternalData[ftype]:
        for idx, tpl in InternalData['info']:
            n, _, _, _ = tpl
            if n == newName:
                InternalData['info'][idx] = [newName, planCycle, filetype, ver]
                break
    else:
        InternalData['files'].append(newName)
        InternalData[ftype].append(newName)
        InternalData['info'].append([newName, planCycle, filetype, ver])

    dname, bname, ext = getFileNameParts(newName)
    InternalData['info_name'][bname] = [planCycle, filetype, ver]

    if planCycle:
        setLastVersionForPlanningCycle(planCycle, filetype, ver)
    saveInternalData()
    pprint(InternalData)

def getFileNameParts(filename):
    """
    Returns the directory, the basename (without ext.) and extension of the
    input filename
    :param filename: The input filename
    :return: (dname, bname, ext)
    """
    filename_noext, ext = os.path.splitext(filename)
    dname = os.path.dirname(filename_noext)
    bname = os.path.basename(filename_noext)
    return dname, bname, ext

def addSRRFDreference(ssrfd, por):
    """
    Add reference to SSR-FD into POR file name
    :param ssrfd: the SSR-FD file name
    :param por: the initial SSR-FD file name
    :return: the new POR file name
    """
    ssrfdDname, ssrfdBname, ssrfdExt = getFileNameParts(ssrfd)
    porDname, porBname, porExt = getFileNameParts(por)
    
    ssrfdPlCl, ssrfdTyp, ssrfdVer = InternalData['info_name'][ssrfdBname]
    porPlCl, porTyp, porVer = InternalData['info_name'][porBname]
    porText = porBname[22:]

    newPor = generateFileName(filetype=porTyp, planCycle=porPlCl, ver=porVer,
                              addPlanCycle=ssrfdPlCl, addVer=ssrfdVer, text=porText)
    return os.path.join(porDname, os.path.basename(newPor))

def copyFileToFolder(filename, folder):
    """
    Copy specified file to folder, returning the possible errors we may get
    :param filename: the filename
    :param folder: the folder
    :return: a error list
    """
    newFile = None
    errors = []
    # Store selected SSR-FD into temp. package folder
    try:
        shutil.copy(filename, folder)
        newFile = os.path.join(folder, os.path.basename(filename))
    except shutil.Error as err:
        errors.extend(err.args[0])
    except OSError as why:
        errors.append((filename, folder, str(why)))

    return newFile, errors

def addCounterToName(filename, filetype, rename=True):
    """
    Add the counter to the end of the filename
    :param filename: the original filename
    :param filetype: the file type
    :param rename: True: rename the file with the new name
    :return: the file name with the corresponding counter added
    """
    dname, bname, ext = getFileNameParts(filename)

    typeId = FileTypeId[filetype.name]
    counter = InternalData['counters'][typeId] + 1
    InternalData['counters'][typeId] = counter
    saveInternalData()

    newName = os.path.join(dname, f'{bname}_{counter:04d}{ext}')
    if rename: shutil.move(filename, newName)

    return newName

def createManifestFile(mFile, files):
    """
    Create manifest file following the Planning Files and Event File
    Interface Control Document (MDS-MCS-SW-ICD-1001-OPS-GD)
    :param mFile: name of the manifest file
    :param files: list of tuples (filename:String, bin:Boolean)
    :return: True if successful, False otherwise
    """
    try:
        with open(mFile, "w") as md5file:
            md5file.write(f'{len(files)}\n')
            for f, isBin in files:
                md5 = md5sum(f)
                binTag = ' *' if isBin else '  '
                bname = os.path.basename(f)
                md5file.write(f'{md5}{binTag}{bname}\n')
        return True
    except:
        return False

def getCRFGFileName(por):
    """
    Generate the CRFG file name
    :param por: Name of the POR file
    """
    dname, bname, ext = getFileNameParts(por)
    prefix, ext = FileTypeInfo[FileType.CRFG.name]
    return os.path.join(dname, f'{prefix}{bname[4:]}.{ext}')

def getSSRFDInfo(filename, get_comments=False):
    """
    Get header info from SSR FD file.
    The assumed struct
        # SSR-FD
        #
        # SOC Generation Date/Time (CCSDS format): <yyyymmddThhmmss>
        # SOC ESS version: <aa.aa>
        # SSR-FD filename: <string of chars>
        # SSR-FD id: <nn>
        # SSR-FD version: <mm>
        #
        # SSR-FD Num Records
        #
        # Earliest Epoch Time
        # Latest Epoch Time
        #
    :param filename: the file name
    :return: a dictionary with the header metadata
    """
    SSRFDHeaderFields = [
        ('date',        'SOC Generation Date/Time', 't'),
        ('ess'        , 'SOC ESS version'         , 's'),
        ('filename'   , 'SSR-FD filename'         , 's'),
        ('id'         , 'SSR-FD id'               , 's'),
        ('version'    , 'SSR-FD version'          , 's'),
        ('num_records', 'SSR-FD Num Records'      , 's'),
        ('start_epoch', 'Earliest Epoch Time'     , 't'),
        ('end_epoch'  , 'Latest Epoch Time'       , 't')
    ]

    info = {}
    addComments = []

    if not os.path.exists(filename):
        return False, info, addComments

    with open(filename, 'r') as fssrfd:
        # Read until SSR-FD is found
        line ='#'
        headerFound = False
        while line and line[0] == '#' and (not headerFound):
            line = fssrfd.readline()
            headerFound = (re.search(r'\A#\s*SSR-FD\s*\B', line) is not None)

        if not headerFound:
            return False, info

        # If header was found, look for header items:
        line ='#'
        info = {}
        endOfHeaderInfo = False
        while line and line[0] == '#' and not endOfHeaderInfo:
            line = fssrfd.readline()
            for item, itemDesc, itemType in SSRFDHeaderFields:
                if itemDesc in line:
                    m = re.search(r':\s*([-:a-zA-Z0-9_.]+)\s*\B', line)
                    text = m.group(1)
                    if itemType == 't' and text[-1] != 'Z': text = f'{text}Z'
                    info[item] = text if itemType == 's' \
                        else (datetime.datetime.strptime(text, "%Y-%m-%dT%H:%M:%SZ")
                              if itemType == 't' else None)
                    endOfHeaderInfo = item == 'end_epoch'
                    break

        if get_comments:
            # Read rest of comments
            line = fssrfd.readline()
            while line and line[0] == '#':
                addComments.append(line)
                line = fssrfd.readline()

    dictHasEntries = bool(info)
    return dictHasEntries, info, addComments

def getSSRFDData(filename):
    """
    Get all the data from a SSR-FD file
    :param filename: The full path and filename of the SSR-FD file
    :return: The columns with the data in the SSR-FD, or None
    """
    data = collections.OrderedDict()

    if not os.path.exists(filename):
        return None

    with open(filename, 'r') as fssrfd:
        # Read until all comments are read
        line ='#'
        while line and line[0] == '#':
            line = fssrfd.readline()

        # Process SSR-FD data columns
        # The expected format is:
        # Column  1: Earliest slew start, Julian date format [TDB, in days since 2000-01-01 00:00:00.000] when slew to target attitude may start, E24.16E3 format (num chars)
        # Column  2: Earliest start time: Human readable time format, yyyymmddThhmmss.mmm (not processed by FD) (num chars)
        # Column  3: Epoch: Julian date format [TDB, in days since 2000-01-01 00:00:00.000] when attitude shall be attained (or time on target), E24.16E3 format (num chars)
        # Column  4: Epoch: Human readable time format, yyyymmddThhmmss.mmm (not processed byFD) (num chars)
        # Column  5: Q1: Quaternion 1
        # Column  6: Q2: Quaternion 2
        # Column  7: Q3: Quaternion 3
        # Column  8: Q4: Quaternion 4 (Scalar part)
        # Column  9: Slew type: Type of slew, I1, [0=No Slew, 1=Dither Slew, 2=Field Slew, 3=Long Slew, 9=SOPSWindowStart]
        # Column  10: Pointing number (0-N)
        # Column 11: SOC_ID: for the dither commanded, H8 (not processed by FD)
        # Column 12: Dither_Activity: Descriptor of the Dither Activity requested used by the SCS, Char8 (not processed by FD)
        # Column 13: Instrument Start Margin: Margin in seconds from the Epoch to the start of instrument activities used by SCS, I6 (not processed by FD)
        data['slew_start'] = []
        data['start_time'] = []
        data['epoch'] = []
        data['epoch_time'] = []
        data['q1'] = []
        data['q2'] = []
        data['q3'] = []
        data['q4'] = []
        data['slew_type'] = []
        data['pointing_number'] = []
        data['soc_id'] = []
        data['dither_activity'] = []
        data['instr_start_margin'] = []
        
        while line:
            items = line.split()

            data['slew_start'].append(float(items[0]))
            data['start_time'].append(datetime.datetime.strptime(f'{items[1]}Z', "%Y-%m-%dT%H:%M:%SZ"))
            data['epoch'].append(float(items[2]))
            data['epoch_time'].append(datetime.datetime.strptime(f'{items[3]}Z', "%Y-%m-%dT%H:%M:%SZ"))
            data['q1'].append(float(items[4]))
            data['q2'].append(float(items[5]))
            data['q3'].append(float(items[6]))
            data['q4'].append(float(items[7]))
            data['slew_type'].append(int(items[8]))
            data['pointing_number'].append(int(items[9]))
            data['soc_id'].append(int(items[10]))
            data['dither_activity'].append(items[11])
            data['instr_start_margin'].append(int(items[12]))

            line = fssrfd.readline()

    return data

def loadSSRFD(filename):
    """
    Load a SSR-FD filename in a structure in memory
    :param filename: The full path and filename of the SSR-FD file
    :return: The structure with the information of the SSR-FD file
    """
    # Read header section
    isOk, info, comments = getSSRFDInfo(filename, get_comments=True)
    if not isOk:
        return None
    
    # Read data section
    data = getSSRFDData(filename)
    if data is None:
        return None
    
    return {"info": info, "comments": comments, "data": data}

def loadPOR(filename):
    """
    Load a POR filename in a structure in memory
    :param filename: The full path and filename of the POR file
    :return: The structure with the information of the POR file
    """
    if not os.path.exists(filename):
        return None

    # Read and parse XML file, resulting in an ElementTree object
    tree = None
    with open(filename, 'rt') as fxml:
        tree = ET.parse(fxml)
    return tree


class breakable:
    class Break(Exception):
        """Break out of the with statement"""

    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value.__enter__()

    def __exit__(self, etype, value, traceback):
        error = self.value.__exit__(etype, value, traceback)
        if etype == self.Break:
            return True
        return error


if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
