<?xml version="1.0" encoding="UTF-8"?>
<!--
=======================================================================
 File: SCS_to_POR.xslt
 Description: XSLT File to convert SCS output to POR CRF File
 Version: 0.1
 Date: 2019/10/14
 Author: J C Gonzalez / Euclid SOC Team
=======================================================================
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="date">
<xsl:output version="1.0" encoding="UTF-8" indent="no"/>

<xsl:param name="quote">"</xsl:param>

<xsl:param name="sequence_name" select="'Sequence'"/>

<!-- Invalid value markers -->
<xsl:param name="invalidInt" select="4"/>
<xsl:param name="invalidDbl" select="2048"/>
<xsl:param name="invalidStr" select="565000"/>

<xsl:variable name="creationTime">
  <xsl:value-of select="date:date-time()"/>
</xsl:variable>

<xsl:variable name="invalidData">0</xsl:variable>

<xsl:variable name="minDateTime">
  <xsl:for-each select="/FP_Schedule_Exported/Data_Block/List_of_Task/Task">
    <xsl:sort select="Start/UTC" order="ascending"/>
    <xsl:if test="position() = 1"><xsl:value-of select="Start/UTC"/></xsl:if>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="maxDateTime">
  <xsl:for-each select="/FP_Schedule_Exported/Data_Block/List_of_Task/Task">
    <xsl:sort select="Start/UTC" order="descending"/>
    <xsl:if test="position() = 1"><xsl:value-of select="Start/UTC"/></xsl:if>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="numMiniSeq">
  <xsl:value-of select="count(/FP_Schedule_Exported/Data_Block/List_of_Task/Task[Task_Type=$sequence_name])"/>
</xsl:variable>

<xsl:template match="/">
<planningData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="euclidPlanningData.xsd">
  <commandRequests>
    <header type="POR" formatVersion="1" fileVersion="1">
      <genTime><xsl:value-of select="$creationTime"/></genTime>
      <validityRange type="absoluteTime">
        <startTime><xsl:value-of select="$minDateTime"/></startTime>
        <endTime><xsl:value-of select="$maxDateTime"/></endTime>
      </validityRange>
    </header>
  <xsl:apply-templates select="FP_Schedule_Exported/Data_Block"/>
  </commandRequests>
</planningData>
  <xsl:if test="$invalidData=1">
    <xsl:message terminate="yes">ERROR: Invalid data found!</xsl:message>
  </xsl:if>
</xsl:template>

<xsl:template match="Data_Block">
  <xsl:text disable-output-escaping="yes">&lt;occurrenceList count=&quot;</xsl:text>
  <xsl:value-of select="$numMiniSeq"/>
  <xsl:text disable-output-escaping="yes">&quot; creationTime=&quot;</xsl:text>
  <xsl:value-of select="$creationTime"/>
  <xsl:text disable-output-escaping="yes">&quot; author="Euclid SOC Team at ESAC"&gt;</xsl:text>
  <xsl:apply-templates select="List_of_Task"/>
  <xsl:text disable-output-escaping="yes">&lt;/occurrenceList&gt;</xsl:text>
</xsl:template>

<xsl:template match="List_of_Task">
  <xsl:for-each select="Task[Task_Type=$sequence_name]">
    <xsl:sort select="Start/UTC"/>
    <xsl:variable name="task_name"><xsl:value-of select="Task_Name"/></xsl:variable>
    <xsl:text disable-output-escaping="yes">&lt;sequence name=&quot;</xsl:text>
    <xsl:value-of select="Task_Name"/>
    <xsl:text disable-output-escaping="yes">&quot;&gt;</xsl:text>
		<uniqueID><xsl:value-of select="Task_Id"/></uniqueID>
		<insertOrDeleteFlag>Insert</insertOrDeleteFlag>
		<source>Euclid SOC</source>
    <executionTime>
      <actionTime><xsl:value-of select="Start/UTC"/></actionTime>
    </executionTime>
    <releaseTime/>
    <description/>
    <xsl:variable name="numOfParams">
      <xsl:value-of select="count(Parameter_List/parameter)"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$numOfParams &lt; 1">
        <parameterList/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">&lt;parameterList count=&quot;</xsl:text>
        <xsl:value-of select="$numOfParams"/>
        <xsl:text disable-output-escaping="yes">&quot;&gt;</xsl:text>
        <xsl:for-each select="Parameter_List/parameter">
          <xsl:variable name="param_num" select="position()"/>
          <xsl:text disable-output-escaping="yes">&lt;parameter name=&quot;</xsl:text>
          <xsl:value-of select="@name"/>
          <xsl:text disable-output-escaping="yes">&quot; position=&quot;</xsl:text>
          <xsl:number value="position()" format="1"/>
          <xsl:text disable-output-escaping="yes">&quot;&gt;</xsl:text>
          <xsl:variable name="error">
            <xsl:choose>
              <xsl:when test="current()=$invalidInt">ERROR: Parameter '<xsl:value-of select="@name"/>' with invalid int value</xsl:when>
              <xsl:when test="current()=$invalidDbl">ERROR: Parameter '<xsl:value-of select="@name"/>' with invalid double value</xsl:when>
              <xsl:when test="current()=$invalidStr">ERROR: Parameter '<xsl:value-of select="@name"/>' with invalid string value</xsl:when>
              <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:if test="$error!=0">
            <xsl:variable name="invalidData">1</xsl:variable>
            <xsl:call-template name="error_message">
              <xsl:with-param name="error" select="$error"/>
              <xsl:with-param name="pos" select="$param_num"/>
              <xsl:with-param name="seq" select="$task_name"/>
            </xsl:call-template>
            <xsl:text disable-output-escaping="yes">&lt;!-- </xsl:text><xsl:value-of select="$error"/> <xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
          </xsl:if>
          <value><xsl:value-of select="."/></value>
          <xsl:text disable-output-escaping="yes">&lt;/parameter&gt;</xsl:text>
        </xsl:for-each>
        <xsl:text disable-output-escaping="yes">&lt;/parameterList&gt;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text disable-output-escaping="yes">&lt;/sequence&gt;</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template name="error_message">
  <xsl:param name="error" select="'ERROR!'"/>
  <xsl:param name="pos" select="0"/>
  <xsl:param name="seq" select="'sequence'"/>
  <xsl:message>
    <xsl:value-of select="$sequence_name"/>
    <xsl:text> </xsl:text><xsl:value-of select="$seq"/>
    <xsl:text>, param #</xsl:text><xsl:value-of select="$pos"/>
    <xsl:text>: </xsl:text>
    <xsl:value-of select="$error"/>
  </xsl:message>
</xsl:template>

</xsl:stylesheet>
