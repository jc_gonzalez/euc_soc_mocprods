#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
import tkinter as tk                # python 3
import tkinter.ttk as ttk           # python 3

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys
from pprint import pprint

from filemng import getSSRFDInfo, FileType
from gui_elements import LabelFlag

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class CompareTwoSSRFD(ttk.Frame):
    """

    """
    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        self.controller = controller

        lfrm1 = ttk.LabelFrame(self, text='Comparison of two SSR-FD files')

        self.file1 = tk.StringVar()
        self.file2 = tk.StringVar()
        self.mode = tk.StringVar()
        modes = ('fs-fs', 'fs-repo')
        self.lblsMode = {modes[0]: ('First SSR-FD file (XML)',
                                    'Second SSR-FD file (XML)'),
                         modes[1]: ('SSR-FD file from file system (XML)',
                                    'SSR-FD from internal repository (XML)')}


        frm10 = ttk.Frame(lfrm1)
        frm11 = ttk.Frame(lfrm1)
        frm12 = ttk.Frame(lfrm1)
        frm13 = ttk.Frame(lfrm1)

        frm10left = ttk.Frame(frm10)
        frm10right = ttk.Frame(frm10)

        ttk.Label(frm10left, text='Select pair of SSR-FD to compare:').\
            pack(side=tk.TOP, anchor=tk.NW, padx=10, pady=2, fill=tk.X)
        frm10left.pack(side=tk.LEFT, anchor=tk.NW, padx=10, pady=5)

        rbtn1 = ttk.Radiobutton(frm10right, text='Two SSR-FD files from filesystem',
                                variable=self.mode, value=modes[0], command=self.setCmpLabels)
        rbtn1.pack(side=tk.TOP, anchor=tk.NW, padx=20, pady=2)

        rbtn2 = ttk.Radiobutton(frm10right, text='SSR-FD from filesystem vs. SSR-FD from internal repository',
                                variable=self.mode, value=modes[1], command=self.setCmpLabels)
        rbtn2.pack(side=tk.TOP, anchor=tk.NW, padx=20, pady=2)
        frm10right.pack(side=tk.RIGHT, padx=10, pady=5)
        frm10.pack(expand=tk.N, fill=tk.X)
        self.mode.set(modes[0])

        frm11left = ttk.Frame(frm11)
        frm11right = ttk.Frame(frm11)

        self.lbl1 = ttk.Label(frm11left, text=self.lblsMode[modes[0]][0])
        self.lbl1.pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        frm11left.pack(side=tk.LEFT, padx=10, pady=5)

        self.edfile1 = ttk.Entry(frm11right, textvariable=self.file1)
        self.edfile1.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        self.btn1 = ttk.Button(frm11right, text='...', width=2,
                   command=lambda: controller.setPath(self.file1, folder=False))
        self.btn1.pack(side=tk.RIGHT, padx=2, pady=2)
        frm11right.pack(side=tk.RIGHT, padx=10, pady=5, expand=tk.YES, fill=tk.X)

        frm11.pack(expand=tk.N, fill=tk.X)

        frm12left = ttk.Frame(frm12)
        frm12right = ttk.Frame(frm12)

        self.lbl2 = ttk.Label(frm12left, text=self.lblsMode[modes[0]][1])
        self.lbl2.pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        frm12left.pack(side=tk.LEFT, padx=10, pady=5)

        self.edfile2 = ttk.Entry(frm12right, textvariable=self.file2)
        self.edfile2.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        self.btn2 = ttk.Button(frm12right, text='...', width=2,
                   command=lambda: controller.setPath(self.file2, folder=False))
        self.btn2.pack(side=tk.RIGHT, padx=2, pady=2)
        frm12right.pack(side=tk.RIGHT, padx=10, pady=5, expand=tk.YES, fill=tk.X)

        frm12.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm13, text='                ').pack(side=tk.LEFT,fill=tk.X, expand=tk.NO)
        self.results = ttk.Frame(frm13)
        self.results.pack(side=tk.LEFT,fill=tk.X, expand=tk.NO)
        frm13.pack(expand=tk.N, fill=tk.X)

        ttk.Label(lfrm1, text='').pack(fill=tk.BOTH, expand=tk.YES)

        btnGo = ttk.Button(lfrm1, text="Compare",
                           command=lambda: self.compare())
        btnBack = ttk.Button(lfrm1, text="<- back", #text="\U000021b2",
                             command=lambda: controller.show_frame("StartPage"))

        btnGo.pack(side=tk.RIGHT, padx=2, pady=10)
        btnBack.pack(side=tk.LEFT, padx=2, pady=10)

        lfrm1.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)

    def setCmpLabels(self):
        """Set labels to select two SSR-FDs from file system, or a SSR-FD
        from filesystem and another from internal repo."""
        mode = self.mode.get()
        labels = self.lblsMode[mode]
        self.lbl1.config(text=labels[0])
        self.lbl2.config(text=labels[1])
        if mode == 'fs-fs':
            self.btn2.config(command=lambda: self.controller.setPath(self.file2, folder=False))
        else:
            self.btn2.config(command=lambda: self.controller.showListOfFiles(FileType.SSRFD, self.file2))

    def compare(self):
        """Use XSLT to convert output from SCS to POR format"""
        logger.info('Performing comparison between SSR-FD files')

        file1 = self.file1.get()
        file2 = self.file2.get()
        msgs = []

        # Get info from first file
        isOk1, info1, _ = getSSRFDInfo(file1)
        if isOk1:
            isOk2, info2, _ = getSSRFDInfo(file2)
            if isOk2:
                # Check ESS versions
                if info1['ess'] != info2['ess']:
                    msgs.append(([f'Both files {file1}',
                                  f'and {file2}',
                                  f'were generated with different versions of ESS ',
                                  f'({info1["ess"]} and {info2["ess"]})'], False))

                # Check planning cycles
                if info1['id'] != info2['id']:
                    msgs.append(([f'Both files {file1}',
                                  f'and {file2}',
                                  f'correspond to different planning cycles ' +
                                  f'({info1["id"]} and {info2["id"]})'], False))
                else:
                    msgs.append(([f'Both files {file1}',
                                  f'and {file2}',
                                  f'correspond to the same ' +
                                  f'planning cycle ({info1["id"]})'], True))

                # Check number of records
                if info1['num_records'] != info2['num_records']:
                    msgs.append(([f'Both files {file1}',
                                  f'and {file2}',
                                  f'have different number of records ' +
                                  f'({info1["num_records"]} and {info2["num_records"]})'], False))
                else:
                    msgs.append(([f'Both files {file1}',
                                  f'and {file2}',
                                  f'contain the same number of records ' +
                                  f'({info1["num_records"]})'], True))

                # Check datetime intervals
                results = checkDatetimeIntervals(info1['start_epoch'], info1['end_epoch'],
                                                 info2['start_epoch'], info2['end_epoch'])
                pprint(msgs)
                pprint(results)
                msgs.extend(results)
            else:
                msgs.append(([f'File {file2}',
                              'does not contain SSR-FD header information'], False))

        else:
            msgs.append(([f'File {file1}',
                          'does not contain SSR-FD header information'], False))

        for child in self.results.winfo_children():
            child.destroy()

        for msg, result in msgs:
            r = LabelFlag(self.results)
            r.set(text=('\n'.join(msg)), flag=result)
            r.pack(fill=tk.BOTH, expand=tk.YES)
            loggerFunc = logger.info if result else logger.warning
            for m in msg: loggerFunc(m)

        logger.info('Done.')

def checkDatetimeIntervals(start1, end1, start2, end2):
    """
    Performs a detailed comparison of date-time intervals of both files
    :param start1: Earliest Epoch Time for file 1
    :param end1: Latest Epoch Time for file 1
    :param start2: Earliest Epoch Time for file 2
    :param end2: Latest Epoch Time for file 2
    :return: a list of (msgs, result)
    """
    msgsTable = [
        [
            (['Time interval for 1st file starts before the time interval for 2nd file.',
              'End of 1st file\'s interval overlaps with beginning of 2nd\'s.',
              'Time interval for 1st file ends earlier than time interval for 2nd file.'], False),
            (['Time interval for 1st file starts before the time interval for 2nd file,',
              'although they end at the same time.',
              'Therefore, 2nd time interval is contained in 1st time interval.'], False),
            (['Time interval for 2nd file is fully contained in time interval of 1st file.'], False)
        ], [
            (['Both time intervals start at the same epoch, but time interval for 2nd file',
              'ends later than the time interval for 1st file.',
              'Therefore, 1st time interval is contained in 2nd time interval.'], False),
            (['Both time intervals are identical.'], True),
            (['Both time intervals start at the same epoch, but time interval for 1st file',
              'ends later than the time interval for 2nd file.',
              'Therefore, 2nd time interval is contained in 1st time interval.'], False)
        ], [
            (['Time interval for 1st file is fully contained in time interval of 2nd file.'], False),
            (['Both time intervals end at the same epoch, but time interval for 1st file',
              'starts later than the time interval for 2nd file.',
              'Therefore, 1st time interval is contained in 2nd time interval.'], False),
            (['Time interval for 1st file starts later the time interval for 2nd file.',
              'Beginning of 1st file\'s interval overlaps with end of 2nd\'s.',
              'Time interval for 2nd file ends earlier than time interval for 1st file.'], False)
        ]
    ]

    msgs = [(['Time intervals of both files are:',
              f' - First file:  {start1.strftime("%Y-%m-%dT%H:%M:%SZ")} - ' + \
              f'{end1.strftime("%Y-%m-%dT%H:%M:%SZ")}',
              f' - Second file: {start2.strftime("%Y-%m-%dT%H:%M:%SZ")} - ' + \
              f'{end2.strftime("%Y-%m-%dT%H:%M:%SZ")}'], True)]

    if end1 < start2 or end2 < start1:
        msgs.append((['Both files specify non-overlapping datetime intervals.'], False))
    else:
        idx1 = 0 if (start1 < start2) else (1 if start1 == start2 else 2)
        idx2 = 0 if (end1 < end2) else (1 if end1 == end2 else 2)
        msgs.append(msgsTable[idx1][idx2])

    return msgs


if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
