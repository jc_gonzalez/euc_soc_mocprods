BEGIN {
    pt="";
}

/POINTING_REQUEST/ {
    n = split($0, a, "[<>.T]");
    if (a[5] == pt) {
        k = 0;
    } else {
        pt = a[5];
        k = 1;
    }
}

/<actionTime>/ {
    n = split($0, a, "[<>]");
    if (k > 0) {
        print pt, a[3];
    }
}
