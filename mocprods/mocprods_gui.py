#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
import tkinter as tk  # python 3
import tkinter.ttk as ttk  # python 3
from tkinter import filedialog
from tkinter import font as tkfont  # python 3

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys
import json
import re

from gui_elements import CustomText

from config import FilesRepository, DBFile, SOCprodsforMOC_image
from conv_to_por import ConvertSCSOutputToPOR
from load_prods import LoadProducts
from cmp_ssrfd import CompareTwoSSRFD
from generate_prods import GenerateProdsForMOC
from tools import createDirIfNotExist
from filemng import initialize_pickle, \
    dumpInternalData, clearInternalData, \
    FileType, getListOfFiles

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.getenv('LOGGING_MODULES','').split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)


class StartPage(ttk.Frame):
    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        self.controller = controller

        self.img = tk.PhotoImage(file=SOCprodsforMOC_image)
        icon = tk.Canvas(self)
        icon.pack(fill=tk.BOTH, expand=tk.YES)
        icon.create_image(100, 20, image=self.img, anchor=tk.NW)

        ttk.Label(self, text='').pack(fill=tk.BOTH, expand=tk.YES) # spacer

        frm = ttk.Frame(self)

        frm1 = ttk.Frame(frm)
        frm2 = ttk.Frame(frm)

        btn1 = ttk.Button(frm1, text="Convert SCS Output to POR",
                           command=lambda: controller.show_frame('ConvertSCSOutputToPOR'))
        btn2 = ttk.Button(frm2, text="Load Products",
                             command=lambda: controller.show_frame('LoadProducts'))
        btn3 = ttk.Button(frm1, text='Compare two SSR-FD files',
                             command=lambda: controller.show_frame('CompareTwoSSRFD'))
        btn4 = ttk.Button(frm2, text='Generate products for MOC',
                             command=lambda: controller.show_frame('GenerateProdsForMOC'))

        btn1.pack(padx=20, pady=10, fill=tk.BOTH, expand=tk.YES)
        btn2.pack(padx=20, pady=10, fill=tk.BOTH, expand=tk.YES)
        btn3.pack(padx=20, pady=10, fill=tk.BOTH, expand=tk.YES)
        btn4.pack(padx=20, pady=10, fill=tk.BOTH, expand=tk.YES)
        ttk.Label(frm, text='').pack(side=tk.LEFT, padx=40, pady=10, fill=tk.BOTH, expand=tk.YES)
        frm1.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        ttk.Label(frm, text='').pack(side=tk.RIGHT, padx=40, pady=10, fill=tk.BOTH, expand=tk.YES)
        frm2.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)

        frm.pack(fill=tk.BOTH, expand=tk.YES)

        ttk.Label(self, text='').pack(fill=tk.BOTH, expand=tk.YES) # spacer


class App(tk.Tk):
    """
    Main application class for the GUI
    """
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the class data members and build the entire GUI
        """
        self.home = os.getenv('HOME')
        self.lstFiles = None

        # Set up repository folder and database
        self.conn = self.initialize()

        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = ttk.Frame(self)
        container.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage,
                  ConvertSCSOutputToPOR,
                  LoadProducts,
                  CompareTwoSSRFD,
                  GenerateProdsForMOC):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            frame.grid(row=0, column=0, sticky="nsew")
            self.frames[page_name] = frame

        self.readConfig()

        self.title('SOC Products for MOC - Generation Tool')
        
        #=== Dialog menu bar

        menu = tk.Menu(self)

        filemenu = tk.Menu(menu)
        filemenu.add_command(label='Convert SCS output to POR',
                             command=lambda: self.show_frame('ConvertSCSOutputToPOR'))
        filemenu.add_command(label='Load Products',
                             command=lambda: self.show_frame('LoadProducts'))
        filemenu.add_command(label='Compare two SSR-FD',
                             command=lambda: self.show_frame('CompareTwoSSRFD'))
        filemenu.add_command(label='Generate products for MOC',
                             command=lambda: self.show_frame('GenerateProdsForMOC'))
        filemenu.add_separator() #----------
        filemenu.add_command(label='Quit', command=self.quit)
        menu.add_cascade(label='Actions', menu=filemenu)

        toolsmenu = tk.Menu(menu)
        toolsmenu.add_command(label='Browse local repository', command=self.browseRepository)
        toolsmenu.add_separator() #----------
        toolsmenu.add_command(label='Dump internal database', command=self.dumpInternalDB)
        toolsmenu.add_command(label='Reset internal database', command=self.clearInternalDB)
        menu.add_cascade(label='Tools', menu=toolsmenu)

        helpmenu = tk.Menu(menu)
        helpmenu.add_command(label='Help...', command=self.help)
        helpmenu.add_separator() #---------
        helpmenu.add_command(label='About...', command=self.about)
        menu.add_cascade(label='Help', menu=helpmenu)

        self.config(menu=menu)

        self.show_frame("StartPage")

        self.update()
        self.minsize(self.winfo_width(), self.winfo_height())

    def initialize(self):
        """
        Initialize DB and repository folder
        :return:
        """
        createDirIfNotExist(FilesRepository)

        dbFileIsInitialized = os.path.isfile(DBFile)
        #conn = sqlite3.connect(DBFile)
        conn = 'pickle'
        if not initialize_pickle(DBFile, dbFileIsInitialized): #initialize_db(conn):
            logger.error("Problem with the initialization of internal database")
            raise Exception("Problem with the initialization of internal database")

        return conn

    def show_frame(self, page_name):
        """Show a frame for the given page name"""
        frame = self.frames[page_name]
        frame.tkraise()

    def setPath(self, var, folder=False, save=False):
        """
        Allow the user to select the ARES Run Time folder
        """
        dialog = (filedialog.askdirectory if folder
            else (filedialog.asksaveasfilename if save
                  else filedialog.askopenfilename))
        dirpath = dialog()
        if dirpath is not None and dirpath != '':
            var.set(dirpath)

    def showLog(self, logInfo):
        """
        Show the log of an execution
        """
        win = tk.Toplevel(self)
        win.title("Execution log . . .")
        log = '\n'.join(logInfo)
        t = CustomText(win, wrap='word', width=65, height=40, borderwidth=20,
                       font=('Courier', 10), relief=tk.FLAT)
        t.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
        t.tag_configure('error', foreground='red')
        t.tag_configure('red', background='red')
        t.tag_configure('cmd', font=('Courier', 10, 'bold'))
        t.insert('1.0', log)
        t.apply_tag(tag='error', pattern='ERROR.*')
        t.apply_tag(tag='cmd', pattern='^>> .*')
        ttk.Button(win, text='Close', command=win.destroy).pack(side=tk.BOTTOM)

    def showListOfFiles(self, filetype:FileType, var):
        """Show list of files of a given type"""
        win = tk.Toplevel(self)
        win.title(f'Select {filetype.name} . . .')

        self.lstFiles = tk.Listbox(win, {'width': 110})
        self.lstFiles.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES,
                           padx=10, pady=10)

        for item in getListOfFiles(filetype):
            self.lstFiles.insert(tk.END, item)

        ttk.Button(win, text='Close', command=win.destroy).pack(side=tk.LEFT,
                           padx=10, pady=10)
        ttk.Button(win, text="Select",
                       command=lambda: self.selectFile(var, win)).pack(side=tk.RIGHT,
                           padx=10, pady=10)

    def browseRepository(self):
        """Show window with panels to browse the list of files in repository"""
        win = tk.Toplevel(self)
        win.title(f'Repository Browser')

        nb = ttk.Notebook(win)

        frm1 = ttk.Frame(nb)
        frm2 = ttk.Frame(nb)

        lstSSRFD = tk.Listbox(frm1, {'width': 110})
        lstSSRFD.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES, padx=10, pady=10)
        for item in getListOfFiles(FileType.SSRFD):
            lstSSRFD.insert(tk.END, item)

        lstPOR = tk.Listbox(frm2, {'width': 110})
        lstPOR.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES, padx=10, pady=10)
        for item in getListOfFiles(FileType.POR):
            lstPOR.insert(tk.END, item)

        nb.add(frm1, text=FileType.SSRFD.name, padding=20)
        nb.add(frm2, text=FileType.POR.name, padding=20)

        nb.pack(padx=10, pady=10)

        ttk.Button(win, text='Close', command=win.destroy).pack(side=tk.LEFT,
                                                                padx=10, pady=10)

    def selectFile(self, var, window):
        """Select a file from the list of files"""
        fileSelectedIdx = self.lstFiles.curselection()
        var.set(self.lstFiles.get(fileSelectedIdx))
        window.destroy()

    def dumpInternalDB(self):
        """
        Show in console the content of the internal database
        :return: -
        """
        dumpInternalData()

    def clearInternalDB(self):
        """
        Show in console the content of the internal database
        :return: -
        """
        clearInternalData()

    def help(self):
        """
        Show a short help information
        """
        win = tk.Toplevel(self)
        win.title("Help")
        hlp = """
        ARES Data Retrieval and Import Tool

        This tool is a front end to the corresponding import and retrieval Python scripts,
        developed to import data in CSV (or XML in the future) format into the ARES Hadoop
        cluster, or retrieve data from it in and save them in the form of FITS files.

        A set of parameters is needed for each operation, as well as a user-defined
        configuration file.  This is further explained below.


        Import data files to ARES

        For importing existing data, in the form of existing CSV files, the following
        information is needed:

        - Directory where the data files are stored or
        - Wildcard template file name
        - Description file (for non-existing parameters mainly)
        - ARES Runtime folder.
        - Import subdirectory where ARES should look for the files to be imported
        - Type of the data in the files (in case it cannot be deduced by the system)

        If ARES Runtime folder is not specified, then ~/ARES_RUNTIME is assumed,
        unless the environment variable ARES_RUNTIME is set.

        Note that when specifying a definition file, the "paramdef|parameter" part of
        the import folder must be omitted.


        Data retrieval

        For the retrieval of parameter data frmo the ARES cluster, in the form of
        FITS files (with Binary Table Extensions), the following information is required:

        - Parameter IDs (initial and final)
        - Alternatively, a set of parameter IDs or names can be used (TBD)
        - Size of ID block (for the split of the FITS files)
        - Start and End data of the data block to be retrieved.

        Note that the start and the end date can be specified in Year-Month-Day or
        Year-Day_of_Year forms.

        With this information, the system retrieves the data requested and stores them
        in Binary Table Extensions in FITS files.  The size of ID block specifies the
        maximum number of parameters stored in a single FITS file.
"""
        hlp = re.sub('\n +', '\n', hlp)  # remove leading whitespace from each line
        t = CustomText(win, wrap='word', width=65, height=40, borderwidth=20,
                       font=('Helvetica', 10), relief=tk.FLAT)
        t.pack(sid='top', fill='both', expand=True)
        t.tag_configure('title', font=('Helvetica', 16, 'bold'), justify=tk.CENTER)
        t.tag_configure('subtitle', font=('Helvetica', 12, 'bold italic'))
        t.tag_configure('centered', justify=tk.CENTER)
        t.tag_configure('emph', font=('Helvetica', 10, 'bold italic'))
        t.tag_configure('blue', foreground='blue')
        t.tag_configure('red', background='red')
        t.insert('1.0', hlp)
        t.apply_tag(tag='title', pattern='^ARES Data Retrieval and Import Tool')
        t.apply_tag(tag='subtitle', pattern='^Import data files to ARES')
        t.apply_tag(tag='subtitle', pattern='^Data retrieval')
        t.apply_tag(tag='blue', pattern='ARES_RUNTIME', )
        t.apply_tag(tag='emph', pattern='(Year-Month-Day|Year-Day_of_Year|CSV|XML| ARES |FITS)')
        t.apply_tag(tag='blue', pattern='^- .*')
        ttk.Button(win, text='OK', command=win.destroy).pack()

    def about(self):
        """
        Show simple About... dialog
        """
        win = tk.Toplevel(self)
        win.title("About")
        t = CustomText(win, wrap='word', width=60, height=9, borderwidth=0,
                       font=('Helvetica', 10))
        t.insert('1.0', 'SOC Products for MOC\n', 'title')
        t.insert(tk.END, 'Generation Tool v 0.1, Oct.2020\n\n', 'subtitle')
        t.insert(tk.END, 'This application is a simple tool that allows the operator to\n')
        t.insert(tk.END, 'handle SSR-FD and POR files, to perform checks on them, and to\n')
        t.insert(tk.END, 'prepare the package files to send them to MOC.\n\n')
        t.insert(tk.END, 'Euclid SOC Team at ESAC\n')
        t.pack(sid='top',fill='both',expand=True)
        t.tag_configure('title', font=('Helvetica', 16, 'bold'))
        t.tag_configure('subtitle', font=('Helvetica', 12, 'bold italic'))
        t.tag_configure('centered', justify=tk.CENTER)
        t.apply_tag(tag='centered', pattern='^.')
        ttk.Button(win, text='OK', command=win.destroy).pack()

    def quit(self):
        """
        Quit the application
        """
        self.destroy()

    def readConfig(self):
        """
        Read application configuration from appropriate .config folder in HOME directory
        """
        pass

    def writeConfig(self):
        """
        Write configuration to appropriate .config folder in HOME directory
        """
        with open(self.cfgFile, 'w') as cfgf:
            json.dump(self.cfgData, cfgf)

    def menuCallback(self, item):
        """
        Callback for the menu bar menu options
        """
        pass

    def greetings(self, msg):
        """
        Says hello
        """
        logging.info('='*60)
        logging.info(msg)
        logging.info('='*60)


def main():
    """
    Main function
    :return: 
    """
    configureLogs()

    app = App()
    app.mainloop()


if __name__ == '__main__':
    main()
