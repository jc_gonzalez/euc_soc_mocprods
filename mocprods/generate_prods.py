#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""

import tkinter as tk                # python 3
import tkinter.ttk as ttk           # python 3

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys
import datetime
import tempfile
import zipfile
import shutil
from gui_elements import LabelTemp
from filemng import FileType, loadSSRFD, loadPOR, \
    copyFileToFolder, createManifestFile, breakable, \
    addCounterToName, addSRRFDreference, getCRFGFileName

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

DateTimeDiffTolerance = datetime.timedelta(milliseconds=50.)

class GenerateProdsForMOC(ttk.Frame):
    """
    Generate Product Package to send to MOC
    """
    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        self.controller = controller

        lfrm1 = ttk.LabelFrame(self, text='Generate Products for MOC')

        self.fileSSRFD = tk.StringVar()
        self.filePOR = tk.StringVar()
        self.outputDir = tk.StringVar()
        self.prodsSendingMode = tk.StringVar()

        frm11 = ttk.Frame(lfrm1)
        frm12 = ttk.Frame(lfrm1)
        frm13 = ttk.Frame(lfrm1)
        frm14 = ttk.Frame(lfrm1)

        ttk.Label(frm11, text='Specify SSR-FD file     ')\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edSSRFDFile = ttk.Entry(frm11, textvariable=self.fileSSRFD)
        self.edSSRFDFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm11, text='...', width=2,
                   command=lambda: controller.showListOfFiles(FileType.SSRFD, self.fileSSRFD))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm11.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm12, text='Specify POR file          ')\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edPORFile = ttk.Entry(frm12, textvariable=self.filePOR)
        self.edPORFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm12, text='...', width=2,
                   command=lambda: controller.showListOfFiles(FileType.POR, self.filePOR))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm12.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm13, text='Output folder for package   ')\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edOutputDir = ttk.Entry(frm13, textvariable=self.outputDir)
        self.edOutputDir.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm13, text='...', width=2,
                   command=lambda: controller.setPath(self.outputDir, folder=True))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm13.pack(expand=tk.N, fill=tk.X, pady=5)

        rbtn0 = ttk.Radiobutton(frm14, text='Send BOTH files SSR-FD & POR',
                                variable=self.prodsSendingMode, value='both')
        rbtn0.pack(side=tk.LEFT, padx=20, pady=10)

        rbtn1 = ttk.Radiobutton(frm14, text='Send only POR (points to SSR-FD)',
                                variable=self.prodsSendingMode, value='por')
        rbtn1.pack(side=tk.RIGHT, padx=20, pady=10)
        frm14.pack(expand=tk.N, fill=tk.X)
        self.prodsSendingMode.set('both')

        btnReset = ttk.Button(lfrm1, text="Reset",
                               command=lambda: self.reset())
        self.btnChecks = ttk.Button(lfrm1, text="Check",
                               command=lambda: self.performChecks())
        self.btnBuild = ttk.Button(lfrm1, text="Build",
                             command=lambda: self.buildPkg())
        self.btnSend = ttk.Button(lfrm1, text="Send",
                             command=lambda: self.sendPkg())
        btnBack = ttk.Button(lfrm1, text="<- back", #text="\U000021b2",
                             command=lambda: controller.show_frame("StartPage"))

        self.lblResult = LabelTemp(lfrm1, 5.0)
        self.lblResult.pack(fill=tk.BOTH, expand=tk.YES)

        ttk.Label(lfrm1, text='').pack(fill=tk.BOTH, expand=tk.YES)

        self.btnSend.pack(side=tk.RIGHT, padx=2, pady=10)
        self.btnBuild.pack(side=tk.RIGHT, padx=2, pady=10)
        self.btnChecks.pack(side=tk.RIGHT, padx=2, pady=10)
        btnReset.pack(side=tk.RIGHT, padx=2, pady=10)
        btnBack.pack(side=tk.LEFT, padx=2, pady=10)

        lfrm1.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)

        self.reset()

    def reset(self):
        """Perform consistency checks on SSR-FD and POR"""
        self.btnBuild.state(["disabled"])
        self.btnSend.state(["disabled"])

    def performChecks(self):
        """Perform consistency checks on SSR-FD and POR"""
        global DateTimeDiffTolerance

        logger.info('Performing SSR-FD and POR consistency checks')

        # Load SSR-FD data
        ssrfdFileName = self.fileSSRFD.get()
        ssrfd = loadSSRFD(ssrfdFileName)
        if ssrfd is None:
            logger.error(f'Error while trying to read {ssrfdFileName}')
            return

        # Load POR data
        porFileName = self.filePOR.get()
        por = loadPOR(porFileName)
        if por is None:
            logger.error(f'Error while trying to read {porFileName}')
            return

        # Get all the pointing identifiers
        lastPtId = ''
        ptNum = 0
        maxDif = datetime.timedelta(milliseconds=0.)
        hasErrors = False

        results = ['>> Checking pointings . . .']
        for seq in por.iter('sequence'):
            pt = seq.find('./uniqueID')
            ptId = pt.text.split('.')[0]
            if ptId != lastPtId:
                lastPtId = ptId
                # Get action time for this pt id. in POR file
                actTime = seq.find('./executionTime/actionTime')
                actDateTime = datetime.datetime.strptime(f'{actTime.text}Z',
                                                         "%Y/%m/%d %H:%M:%S.%fZ")
                # Get corresponding pt time
                ssrfdDateTime = ssrfd['data']['start_time'][ptNum]

                ptNum = ptNum + 1
                difDateTime = abs(actDateTime - ssrfdDateTime)
                if difDateTime > maxDif:
                    maxDif = difDateTime
                if difDateTime > DateTimeDiffTolerance:
                    results.append(f'  Pt. #{ptNum:3d}: {ptId} at {actDateTime} <> {ssrfdDateTime} : {maxDif}')

        # Perform checks:

        # 1. Check number of points
        results.append('>> Performing number of pointings check . . .')
        if ptNum != int(ssrfd['info']['num_records']):
            results.append('  ERROR: Mismatch in number of pointings: ' +
                           f'SSR-FD:{ssrfd["info"]["num_records"]} <> POR:{ptNum}')
            hasErrors = True

        # 2. Check max. dif. in time stamps
        results.append('>> Performing time-stamps max. dif. allowed tolerance check . . .')
        if maxDif > DateTimeDiffTolerance:
            results.append('  ERROR: Difference in time stamps is larger that allowed tolerance: ' +
                           f'max. dif. = {maxDif}')
            hasErrors = True

        # Show results
        if hasErrors:
            msg = 'ERROR when performing consistency checks on SSR-FD and POR'
            self.lblResult.config(foreground='#AA0000')
            self.lblResult.set(msg)
            results.append(msg)
            logger.error(msg)
        else:
            msg = 'Consistency checks successfully passed.'
            self.lblResult.config(foreground='#00AA00')
            self.lblResult.set('Consistency checks successfully passed.')
            results.append(msg)
            logger.info(msg)

        for msg in results:
            logger.error(msg)

        self.controller.showLog(results)

        logger.info('Done.')

        # Allow pass to next step, is successful
        if not hasErrors:
            self.btnBuild.state(["!disabled"])
            self.btnSend.state(["disabled"])

    def buildPkg(self):
        """Build package with files and mode selection"""
        logger.info('Building package for delivery to MOC')

        hasErrors = False
        errors = []
        pkgSSRFDfile = None
        pkgPORfile = None
        pkgMANfile = None
        pkgCRFGFile = None

        # Create temporary folder for package
        with breakable(tempfile.TemporaryDirectory()) as tmpdirname:

            # Copy SSR-FD if needed
            if self.prodsSendingMode.get() == 'both':
                pkgSSRFDfile, errors = copyFileToFolder(self.fileSSRFD.get(), tmpdirname)
                if errors: raise breakable.Break
                pkgSSRFDfile = addCounterToName(pkgSSRFDfile, FileType.SSRFD)
            else:
                pkgSSRFDfile = self.fileSSRFD.get()

            # Copy POR file
            pkgPORfile, errors = copyFileToFolder(self.filePOR.get(), tmpdirname)
            if errors: raise breakable.Break
            if self.prodsSendingMode.get() == 'por':
                newname = addSRRFDreference(pkgSSRFDfile, pkgPORfile)
                shutil.move(pkgPORfile, newname)
                pkgPORfile = newname

            # Generate POR Manifest (MAN) file
            pkgMANfile = os.path.join(tmpdirname, f'MAN{os.path.basename(pkgPORfile)[3:]}')
            if not createManifestFile(pkgMANfile, [(pkgPORfile, False)]):
                errors = [f'Could not create Manifest file {pkgMANfile}']
                raise breakable.Break

            # if everything OK until now, create zip file (CRFG)

            pkgCRFGfile = getCRFGFileName(pkgPORfile)
            pkgPORfile = addCounterToName(pkgPORfile, FileType.POR)
            pkgMANfile = addCounterToName(pkgMANfile, FileType.MANIFEST)

            pkgCRFGfile = addCounterToName(pkgCRFGfile, FileType.CRFG, False)

            try:
                with zipfile.ZipFile(pkgCRFGfile, mode='w') as zf:
                    for fn in [pkgPORfile, pkgMANfile]:
                        logger.info(f'Adding file {os.path.basename(fn)} to package . . .')
                        zf.write(fn, os.path.basename(fn))
            except Exception as ee:
                errors = [f'Could not create zip (CRFG) file {pkgCRFGfile}: {str(ee)}']
                raise breakable.Break

            # Finally, move SSR-FD (if used) and CRFG to target folder
            tgtFolder = self.outputDir.get()
            if self.prodsSendingMode.get() == 'both':
                logger.info(f'Placing SSR-FD file {os.path.basename(pkgSSRFDfile)} into target folder')
                shutil.move(pkgSSRFDfile, tgtFolder)
            logger.info(f'Placing package file {os.path.basename(pkgCRFGfile)} into target folder')
            shutil.move(pkgCRFGfile, tgtFolder)

        if errors:
            for err in errors:
                logger.error(err)
            self.lblResult.config(foreground='#AA0000')
            self.lblResult.set(errors[-1])
        else:
            self.lblResult.config(foreground='#00AA00')
            self.lblResult.set('Package suiccessfully built, and prepared to be sent.')
            self.btnSend.state(["!disabled"])

        logger.info('Done.')

    def sendPkg(self):
        """Sends package to SIS for delivery to MOC"""
        logger.info('Sending package to MOC')
        logger.info('Done.')


if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
