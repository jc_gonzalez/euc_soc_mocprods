#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""

import tkinter as tk                # python 3
import tkinter.ttk as ttk           # python 3
from tkinter import font as tkfont  # python 3

PYTHON2 = False
PY_NAME = "python3"
STRING = str

# import other useful classes
import os, sys
from filemng import FileType, getPlanningCycleFromFileToLoad, getNextVersionForPlanningCycle, \
    storeFile

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------


class LoadProducts(ttk.Frame):
    """
    Load the products (SSR-FD, POR and even Events file) into the internal
    repository
    """
    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        self.controller = controller

        self.loadSSRFD = tk.BooleanVar()
        self.fileSSRFD = tk.StringVar()
        self.ssrfdPlanCycle = tk.IntVar()
        self.ssrfdVersion = tk.IntVar()
        self.ssrfdText = tk.StringVar()

        self.loadPOR = tk.BooleanVar()
        self.filePOR = tk.StringVar()
        self.porPlanCycle = tk.IntVar()
        self.porVersion = tk.IntVar()
        self.porText = tk.StringVar()

        self.loadEvents = tk.BooleanVar()
        self.fileEvents = tk.StringVar()

        self.reset()
        #self.bind("<FocusOut>", self.handle_focus)

        lfrm1 = ttk.LabelFrame(self, text='Load products into local repository')

        lfrm11 = ttk.LabelFrame(lfrm1, text='SSR-FD file')
        lfrm12 = ttk.LabelFrame(lfrm1, text='Events file (ESS)')
        lfrm13 = ttk.LabelFrame(lfrm1, text='POR file')

        frm11 = ttk.Frame(lfrm11)
        frm11b = ttk.Frame(lfrm11)
        frm11c = ttk.Frame(lfrm11)
        frm13 = ttk.Frame(lfrm13)
        frm13b = ttk.Frame(lfrm13)
        frm13c = ttk.Frame(lfrm13)
        frm12 = ttk.Frame(lfrm12)

        ttk.Checkbutton(frm11, text='Select SSR-FD file',
                               variable=self.loadSSRFD,
                               onvalue=True, offvalue=False)\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edInputFile = ttk.Entry(frm11, textvariable=self.fileSSRFD)
        self.edInputFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm11, text='...', width=2,
                   command=lambda: self.setPath('ssrfd'))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm11.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm11b, text='             ').pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        ttk.Label(frm11b, text='         SSR-FD Plan.Cycle', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edSsrfdPlanCycle = ttk.Entry(frm11b, textvariable=self.ssrfdPlanCycle)
        self.edSsrfdPlanCycle.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.NO, fill=tk.X)

        ttk.Label(frm11b, text='   SSR-FD Version', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edSsrfdVersion = ttk.Entry(frm11b, textvariable=self.ssrfdVersion)
        self.edSsrfdVersion.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.NO, fill=tk.X)

        frm11b.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm11c, text='             ').pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        ttk.Label(frm11c, text='         Optional text', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edSsrfdText = ttk.Entry(frm11c, textvariable=self.ssrfdText)
        self.edSsrfdText.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.YES, fill=tk.X)

        frm11c.pack(expand=tk.N, fill=tk.X)

        #ttk.Separator(lfrm1, orient=tk.HORIZONTAL, style="TSeparator").pack(fill=tk.BOTH, expand=tk.YES)

        ttk.Checkbutton(frm13, text='Select POR file     ',
                               variable=self.loadPOR,
                               onvalue=True, offvalue=False)\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edOutputFile = ttk.Entry(frm13, textvariable=self.filePOR)
        self.edOutputFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm13, text='...', width=2,
                   command=lambda: self.setPath('por'))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm13.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm13b, text='             ').pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        ttk.Label(frm13b, text='         POR Plan.Cycle', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edPorPlanCycle = ttk.Entry(frm13b, textvariable=self.porPlanCycle)
        self.edPorPlanCycle.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.NO, fill=tk.X)

        ttk.Label(frm13b, text='   POR Version', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edPorVersion = ttk.Entry(frm13b, textvariable=self.porVersion)
        self.edPorVersion.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.NO, fill=tk.X)

        frm13b.pack(expand=tk.N, fill=tk.X)

        ttk.Label(frm13c, text='             ').pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        ttk.Label(frm13c, text='         Optional text', font=tkfont.Font(size=10))\
            .pack(side=tk.LEFT, fill=tk.X, expand=tk.YES)
        self.edPorText = ttk.Entry(frm13c, textvariable=self.porText)
        self.edPorText.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.YES, fill=tk.X)

        frm13c.pack(expand=tk.N, fill=tk.X)

        #ttk.Separator(lfrm1, orient=tk.HORIZONTAL, style="TSeparator").pack(fill=tk.BOTH, expand=tk.YES)

        ttk.Checkbutton(frm12, text='Select Events file ',
                               variable=self.loadEvents,
                               onvalue=True, offvalue=False)\
                 .pack(side=tk.LEFT, padx=10, pady=2, fill=tk.X)
        self.edOutputFile = ttk.Entry(frm12, textvariable=self.fileEvents)
        self.edOutputFile.pack(side=tk.LEFT, padx=2, pady=2, expand=tk.Y, fill=tk.X)
        ttk.Button(frm12, text='...', width=2,
                   command=lambda: self.setPath('events'))\
           .pack(side=tk.RIGHT, padx=2, pady=2)
        frm12.pack(expand=tk.N, fill=tk.X)

        lfrm11.pack(side=tk.TOP, padx=5, pady=10, fill=tk.BOTH, expand=tk.YES)
        lfrm13.pack(side=tk.TOP, padx=5, pady=10, fill=tk.BOTH, expand=tk.YES)
        lfrm12.pack(side=tk.TOP, padx=5, pady=10, fill=tk.BOTH, expand=tk.YES)

        btnGo = ttk.Button(lfrm1, text="Load",
                           command=lambda: self.loadFiles())
        btnReset = ttk.Button(lfrm1, text="Reset",
                           command=lambda: self.reset())
        btnBack = ttk.Button(lfrm1, text="<- back", #text="\U000021b2",
                             command=lambda: controller.show_frame("StartPage"))

        ttk.Label(lfrm1, text='').pack(fill=tk.BOTH, expand=tk.YES)

        btnGo.pack(side=tk.RIGHT, padx=2, pady=10)
        btnReset.pack(side=tk.RIGHT, padx=2, pady=10)
        btnBack.pack(side=tk.LEFT, padx=2, pady=10)

        lfrm1.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)

    def handle_focus(self, event):
        if event.widget == self:
            self.reset()
            print("I have gained the focus")

    def reset(self):
        """Reset the variables from the edit line widgets"""
        self.loadSSRFD.set(False)
        self.fileSSRFD.set('')
        self.ssrfdPlanCycle.set(0)
        self.ssrfdVersion.set(0)
        self.ssrfdText.set('')

        self.loadPOR.set(False)
        self.filePOR.set('')
        self.porPlanCycle.set(0)
        self.porVersion.set(0)
        self.porText.set('')

        self.loadEvents.set(False)
        self.fileEvents.set('')

    def setPath(self, ftype):
        """Allows the user to select a filename"""
        if ftype in ('por', 'ssrfd'):
            if ftype == 'por':
                fileType = FileType.POR
                varFile = self.filePOR
                varPlanCycle = self.porPlanCycle
                varVersion = self.porVersion
            else:
                fileType = FileType.SSRFD
                varFile = self.fileSSRFD
                varPlanCycle = self.ssrfdPlanCycle
                varVersion = self.ssrfdVersion
            print(varFile.get())
            planCycle = getPlanningCycleFromFileToLoad(varFile.get())
            version = getNextVersionForPlanningCycle(planCycle, fileType)
            varPlanCycle.set(planCycle)
            varVersion.set(version)
        else:
            varFile = self.fileEvents

        self.controller.setPath(varFile, folder=False)

    def loadFiles(self):
        """Load files into local repository"""
        if self.loadSSRFD.get(): self.loadSSRFDFile()
        if self.loadEvents.get(): self.loadESSEventsFile()
        if self.loadPOR.get(): self.loadPORFile()

        logger.info('Done.')

    def loadSSRFDFile(self):
        """Load SSR-FD into local repository"""
        logger.info('Loading SSR-FD file into local repository')
        storeFile(self.fileSSRFD.get(), FileType.SSRFD,
                  planCycle=self.ssrfdPlanCycle.get(),
                  ver=self.ssrfdVersion.get(),
                  text=self.ssrfdText.get())

    def loadPORFile(self):
        """Load POR into local repository"""
        logger.info('Loading POR file into local repository')
        storeFile(self.filePOR.get(), FileType.POR,
                  planCycle=self.porPlanCycle.get(),
                  ver=self.porVersion.get(),
                  text=self.porText.get())

    def loadESSEventsFile(self):
        """Load Events file (from ESS) into local repository"""
        logger.info('Loading Events file (from ESS) into local repository')
        storeFile(self.fileEvents.get(), FileType.EVENTS)

if __name__ == '__main__':
    print('ERROR: This script is not intended to be executed in stand-alone mode.')
